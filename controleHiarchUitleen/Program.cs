﻿using System;
using System.Collections.Generic;
using System.Text;
using Algemeen;
using System.IO;

namespace ControleHiarch
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dtVanDate = Proces.dtStringToDate(args[0]);
            DateTime dtEindDate = Proces.dtStringToDate(args[1]);


            foreach (string sFilePath in Directory.GetFiles(Parameter.SSbRoot + "\\vst\\", "hi*.bes"))
            {

                FileInfo fi = new FileInfo(sFilePath);
                if (!fi.Name.Contains("hiarch"))
                {
                    string sAfdRecNum = fi.Name.Substring(2, 4);
                    string sJaar = fi.Name.Substring(8, 2);
                    string sDag = fi.Name.Substring(10, 3);

                    int iJaar = 0;
                    int iDag = 0;

                    if (Int32.TryParse(sJaar, out iJaar) && Int32.TryParse(sDag, out iDag))
                    {
                        HiarchFile hf = new HiarchFile();
                        hf.sAfdRecNum = sAfdRecNum;
                        DateTime dt = new DateTime(2000 + iJaar, 1, 1);
                        dt = dt.AddDays(iDag - 1);
                        hf.dtStart = dt;
                        hf.sInhoud = Proces.SLeesFile(fi);
                        HiarchFile.lHiarch.Add(hf);
                    }
                }

            }
            HiarchFile.lHiarch.Sort(
                delegate(HiarchFile hf1, HiarchFile hf2)
                {
                    int compareAfd = hf1.sAfdRecNum.CompareTo(hf2.sAfdRecNum);
                    if (compareAfd == 0)
                    {
                        return hf1.dtStart.CompareTo(hf2.dtStart);
                    }
                    return compareAfd;
                }
            );
            for (int i = 0; i < HiarchFile.lHiarch.Count - 1; i++)
            {
                if (HiarchFile.lHiarch[i + 1].sAfdRecNum == HiarchFile.lHiarch[i].sAfdRecNum)
                {
                    HiarchFile.lHiarch[i].dtEind = HiarchFile.lHiarch[i + 1].dtStart;
                }
            }
            Dictionary<string, List<string>> dslKpWissel = Proces.DlsSqlSel("recnr, van_date, afdkod, wplekkod, badnum from proj_regi where afdkod > 0 and wplekkod > 0 and van_date >= '" + dtVanDate.ToString("yyMMdd") + "' and van_date <= '" + dtEindDate.ToString("yyMMdd") + "'");

            string sOutput = string.Empty;
            Dictionary<string, List<string>> dslAfdeling = Proces.DlsSqlSel("afdkod,recnr from afdeling");
            List<List<string>> llsOutput = new List<List<string>>();
            List<string> lsHeader = new List<string>();
            lsHeader.Add("badnum");
            lsHeader.Add("salnum");
            lsHeader.Add("naam");
            lsHeader.Add("init");
            lsHeader.Add("afdkod");
            lsHeader.Add("wplekkod");
            lsHeader.Add("datum");
            lsHeader.Add("afdkod klok");
            lsHeader.Add("wplekkod klok");
            llsOutput.Add(lsHeader);
            Dictionary<string, List<string>> dsPers = Proces.DlsSqlSel("badnum, salnum, naam, init, afdkod, wplekkod from personeel");
            foreach (KeyValuePair<string, List<string>> kvp in dslKpWissel)
            {
                if (dslAfdeling.ContainsKey(kvp.Value[1]) && kvp.Value[0] != "van_date" && dsPers.ContainsKey(kvp.Value[3]))
                {
                    string sAfdRecnum = dslAfdeling[kvp.Value[1]][0];
                    DateTime dt = Proces.dtStringToDate(kvp.Value[0]);
                    HiarchFile hf = HiarchFile.HfFindHiarch(sAfdRecnum, dt);
                    if (!(hf != null && hf.sInhoud.Contains(kvp.Value[2])) && kvp.Value[1].Trim() != "afdkod")
                    {

                        List<string> lsOutput = new List<string>();
                        lsOutput.Add(kvp.Value[3]);
                        lsOutput.Add(dsPers[kvp.Value[3]][0]);
                        lsOutput.Add(dsPers[kvp.Value[3]][1]);
                        lsOutput.Add(dsPers[kvp.Value[3]][2]);
                        lsOutput.Add(dsPers[kvp.Value[3]][3]);
                        lsOutput.Add(dsPers[kvp.Value[3]][4]);
                        lsOutput.Add(dt.ToString("dd-MM-yyyy"));
                        lsOutput.Add(kvp.Value[1]);
                        lsOutput.Add(kvp.Value[2]);
                        llsOutput.Add(lsOutput);
                    }
                }
            }
            FileInfo fi2 = Proces.FiListToCsv(llsOutput);
            fi2.CopyTo(Parameter.SSbRoot + "\\log\\ControleHiarch.csv");
            //Log.Verwerking(fi2.Name);
            //Proces.FiListToGrid("Wrong Afdkod/Wplekkod combination", llsOutput);
            Proces.VerwijderWrkFiles();
        }


    }
    class HiarchFile
    {
        public string sAfdRecNum;
        public DateTime dtStart;
        public DateTime dtEind = new DateTime(2999, 12, 31);
        public static List<HiarchFile> lHiarch = new List<HiarchFile>();
        public string sInhoud;

        public static HiarchFile HfFindHiarch(string sAfdRecNum, DateTime dt)
        {
            HiarchFile result = lHiarch.Find(
            delegate(HiarchFile hf)
            {
                return hf.sAfdRecNum.Contains(sAfdRecNum) && hf.dtStart <= dt && hf.dtEind > dt;
            }
            );
            return result;
        }
    }
}
